<?php
/**
 * @file
 * views_floorplan_example.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function views_floorplan_example_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function views_floorplan_example_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function views_floorplan_example_node_info() {
  $items = array(
    'floorplan' => array(
      'name' => t('Floorplan'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'floorplan_point' => array(
      'name' => t('Floorplan point'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
