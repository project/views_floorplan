<?php
/**
 * @file
 * views_floorplan_example.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_floorplan_example_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'example_floorplan';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Example Floorplan';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Example Floorplan';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'floorplan';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['image'] = 'field_floorplan_image';
  $handler->display->display_options['style_options']['x_axis'] = 'field_x_position';
  $handler->display->display_options['style_options']['y_axis'] = 'field_y_position';
  $handler->display->display_options['style_options']['optional_fields'] = array(
    'label' => 'field_label',
    'color_field' => 'field_color',
    'width' => '800px',
    'height' => '',
    'legend_category_attach' => 'none',
    'legend_category' => 'field_floorplan_image',
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<h3>No Floorplan found</h3>
<p>To use the Example Floorplan view, please do the following:</p>
<ol>
<li>Create one node of type "Floorplan."</li>
<li>Create nodes of type "Floorplan Point" which reference the Floorplan node.</li>
<li>Visit this page, and append the NID of the floorplan node to the end of the address so it reads example-floorplan/(nid).
</ol>';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_floorplan_node']['id'] = 'reverse_field_floorplan_node';
  $handler->display->display_options['relationships']['reverse_field_floorplan_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_floorplan_node']['field'] = 'reverse_field_floorplan_node';
  /* Field: Content: Floorplan Image */
  $handler->display->display_options['fields']['field_floorplan_image']['id'] = 'field_floorplan_image';
  $handler->display->display_options['fields']['field_floorplan_image']['table'] = 'field_data_field_floorplan_image';
  $handler->display->display_options['fields']['field_floorplan_image']['field'] = 'field_floorplan_image';
  $handler->display->display_options['fields']['field_floorplan_image']['label'] = '';
  $handler->display->display_options['fields']['field_floorplan_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_floorplan_image']['alter']['text'] = '[field_floorplan_image-fid]';
  $handler->display->display_options['fields']['field_floorplan_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_floorplan_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_floorplan_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Color */
  $handler->display->display_options['fields']['field_color']['id'] = 'field_color';
  $handler->display->display_options['fields']['field_color']['table'] = 'field_data_field_color';
  $handler->display->display_options['fields']['field_color']['field'] = 'field_color';
  $handler->display->display_options['fields']['field_color']['relationship'] = 'reverse_field_floorplan_node';
  $handler->display->display_options['fields']['field_color']['label'] = '';
  $handler->display->display_options['fields']['field_color']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_color']['type'] = 'list_key';
  /* Field: Content: X-position */
  $handler->display->display_options['fields']['field_x_position']['id'] = 'field_x_position';
  $handler->display->display_options['fields']['field_x_position']['table'] = 'field_data_field_x_position';
  $handler->display->display_options['fields']['field_x_position']['field'] = 'field_x_position';
  $handler->display->display_options['fields']['field_x_position']['relationship'] = 'reverse_field_floorplan_node';
  $handler->display->display_options['fields']['field_x_position']['label'] = '';
  $handler->display->display_options['fields']['field_x_position']['element_type'] = 'strong';
  $handler->display->display_options['fields']['field_x_position']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_x_position']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Y-position */
  $handler->display->display_options['fields']['field_y_position']['id'] = 'field_y_position';
  $handler->display->display_options['fields']['field_y_position']['table'] = 'field_data_field_y_position';
  $handler->display->display_options['fields']['field_y_position']['field'] = 'field_y_position';
  $handler->display->display_options['fields']['field_y_position']['relationship'] = 'reverse_field_floorplan_node';
  $handler->display->display_options['fields']['field_y_position']['label'] = '';
  $handler->display->display_options['fields']['field_y_position']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_y_position']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Label */
  $handler->display->display_options['fields']['field_label']['id'] = 'field_label';
  $handler->display->display_options['fields']['field_label']['table'] = 'field_data_field_label';
  $handler->display->display_options['fields']['field_label']['field'] = 'field_label';
  $handler->display->display_options['fields']['field_label']['relationship'] = 'reverse_field_floorplan_node';
  $handler->display->display_options['fields']['field_label']['label'] = '';
  $handler->display->display_options['fields']['field_label']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'floorplan' => 'floorplan',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'example-floorplan';
  $handler->display->display_options['menu']['title'] = 'Floorplan Example';
  $handler->display->display_options['menu']['description'] = 'Floorplan Example';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_floorplan_image' => 'field_floorplan_image',
    'field_color' => 'field_color',
    'field_label' => 'field_label',
    'field_x_position' => 'field_x_position',
    'field_y_position' => 'field_y_position',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_floorplan_image' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_color' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_label' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_x_position' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_y_position' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'example-floorplan-table';
  $export['example_floorplan'] = $view;

  return $export;
}
