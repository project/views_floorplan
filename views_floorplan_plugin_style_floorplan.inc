<?php
/**
 * @file
 * Contains the floorplan plugin.
 */

/**
 * The plugin that handles the map style.
 */
class ViewsFloorplanPluginStyleFloorplan extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['default_color'] = array('default' => '000000');
    $options['image'] = array('default' => 'none');
    $options['x_axis'] = array('default' => 'none');
    $options['y_axis'] = array('default' => 'none');
    $options['optional_fields']['dropdown_label'] = array('default' => '');
    $options['optional_fields']['persistent_label'] = array('default' => '');
    $options['optional_fields']['width'] = array('default' => '800px');
    $options['optional_fields']['height'] = array('default' => '600px');
    $options['optional_fields']['color_field'] = array('default' => '');
    $options['optional_fields']['legend_category'] = array('default' => '');
    $options['optional_fields']['legend_category_attach'] = array('default' => 'none');

    return $options;
  }
  /**
   * Options form for view.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $fields = array('none' => '- None -');
    $fs = $this->display->handler->get_field_labels();
    foreach ($fs as $key => $f) {
      $fields[$key] = $f;
    }

    // Required fields.
    $form['image'] = array(
      '#type' => 'select',
      '#title' => t('Image FID'),
      '#description' => t('The floorplan image fid will come from this 
      field, from the item in the first row. This field must output as 
      an fid, not an image.'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->options['image'],
    );
    $form['x_axis'] = array(
      '#type' => 'select',
      '#title' => t('X-distance Field'),
      '#description' => t('Determines distance from the left in the 
      form of a percentage.'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->options['x_axis'],
    );
    $form['y_axis'] = array(
      '#type' => 'select',
      '#title' => t('Y-distance Field'),
      '#description' => t('Determines distance from the bottom in the 
      form of a percentage.'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->options['y_axis'],
    );
    $form['default_color'] = array(
      '#title' => t('Default Color'),
      '#description' => t('Enter the default color of the floorplan 
      points in hexadecimal (i.e. 000000 = black, FFFFFF = white, 
      FF0000 = red, 00FF00 = green, 0000FF = blue). This color will be
      used when a color from the color field is not available.'),
      '#type' => 'textfield',
      '#size' => '6',
      '#required' => TRUE,
      '#default_value' => $this->options['default_color'],
    );

    // Non-required fields.
    $form['optional_fields'] = array(
      '#title' => t('Optional Settings'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['optional_fields']['dropdown_label'] = array(
      '#type' => 'select',
      '#title' => t('Dropdown Label Field'),
      '#description' => t('Adds a dropdwon label to the point in the form of an HTML title element.'),
      '#options' => $fields,
      '#default_value' => $this->options['optional_fields']['dropdown_label'],
    );
    $form['optional_fields']['persistent_label'] = array(
      '#type' => 'select',
      '#title' => t('Persistent Label Field'),
      '#description' => t('Adds a label which is external to the dropdown label.'),
      '#options' => $fields,
      '#default_value' => $this->options['optional_fields']['persistent_label'],
    );
    $form['optional_fields']['color_field'] = array(
      '#type' => 'select',
      '#title' => t('Color Field'),
      '#description' => t('Determines what color the floorplan point 
      should be. Expressed as a hexadecimal color i.e. black = 000000.'),
      '#options' => $fields,
      '#default_value' => $this->options['optional_fields']['color_field'],
    );
    // Floorplan settings.
    $form['optional_fields']['width'] = array(
      '#title' => t('Width'),
      '#description' => t('Enter the width of the floorplan image as a CSS 
      property e.g. 600px, 10em, or 100%.'),
      '#type' => 'textfield',
      '#size' => '6',
      '#default_value' => $this->options['optional_fields']['width'],
    );
    $form['optional_fields']['height'] = array(
      '#title' => t('Height'),
      '#description' => t('Enter the height of the floorplan image as a CSS 
      property e.g. 600px or 10em. Height normally does not peform well
      with percentages.'),
      '#type' => 'textfield',
      '#size' => '6',
      '#default_value' => $this->options['optional_fields']['height'],
    );

    // Floorplan Legend settings.
    $form['optional_fields']['legend_category_attach'] = array(
      '#type' => 'select',
      '#title' => t('Map Legend Attachment Position'),
      '#description' => t('If you would like to generate a map legend, note 
      where you would like to attach it.'),
      '#options' => array(
        'none' => t('- Do Not Attach -'),
        'top_collapse' => t('Top - Collapsible'),
        'bottom_collapse' => t('Bottom - Collapsible'),
      ),
      '#default_value' => $this->options['optional_fields']['legend_category_attach'],
    );
    $form['optional_fields']['legend_category'] = array(
      '#type' => 'select',
      '#title' => t('Map Legend Category Field'),
      '#description' => t('If you would like to generate a map legend, note 
      the category here. Map elements may be changed in the custom CSS file.'),
      '#options' => $fields,
      '#default_value' => $this->options['optional_fields']['legend_category'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[optional_fields][legend_category_attach]"]' => array(
            'value' => 'none',
          ),
        ),
      ),
    );
  }

  /**
   * Creates the floorplan map legend.
   *
   * Returns a rendered map if requestsed, NULL otherwise.
   */
  public function createLegend($results, $view_options) {
    // First, check if valid, return NULL otherwise.
    if (isset($view_options['optional_fields']['legend_category']) &&
    isset($view_options['optional_fields']['color_field']) &&
    isset($view_options['default_color']) &&
    isset($view_options['optional_fields']['legend_category_attach']) &&
    $view_options['optional_fields']['legend_category_attach'] !== 'none'
    ) {
      // Build the array of colors and legend items.
      $legend_data = array();
      foreach ($results as $result) {
        if (isset($result[$view_options['optional_fields']['legend_category']]) &&
        isset($result[$view_options['optional_fields']['color_field']])
        ) {
          $legend_data[$result[$view_options['optional_fields']['legend_category']]] = $result[$view_options['optional_fields']['color_field']];
        }
        // If the color isn't set, use the default.
        elseif (isset($result[$view_options['optional_fields']['legend_category']])) {
          $legend_data[$result[$view_options['optional_fields']['legend_category']]] = $view_options['default_color'];
        }
      }

      // Now that we have the colors, generate the legend.
      $legend = '';

      // Add collapse button if a collapsible option was chosen.
      if (strpos($view_options['optional_fields']['legend_category_attach'], 'collapse') !== FALSE) {
        $legend .= '<div class="floorplan-legend-collapse-button">Show/Hide Legend</div>
        <div style="height:0;"></div>';
      }

      $legend .= '<table class="floorplan-legend ' .
      $view_options['optional_fields']['legend_category_attach'] . '">
      <tr>
      <th class="floorplan-legend-color">Color</th>
      <th class="floorplan-legend-category">Category</th>
      </tr>';

      foreach ($legend_data as $label => $color) {
        $color = str_replace('#', '', $color);
        dpm($color);
        $legend .= '<tr>
        <td class="floorplan-legend-color"><div class="floorplan-point" 
        style="background-color:#' . $color . ';"></div></td>
        <td class="floorplan-legend-category">' . $label . '</td>
        </tr>';
      }

      $legend .= '</table>';

      // Return the generated legend.
      return $legend;
    }

    // If the settings were not valid or the , return NULL.
    else {
      return NULL;
    }
  }

  /**
   * Render the given style.
   */
  public function render() {
    $data = $this->view->result;
    $result = $this->render_fields($data);
    $options = $this->options;
    $options['optional_fields']['width'] = $options['optional_fields']['width'] ? $options['optional_fields']['width'] : '100%';
    $legend = $this->createLegend($result, $options);
    /* Create the map background: we have a relatively positioned div
     * with an image background, with points as child divs with
     * absolute positioning.
     */
    $image_data = file_load($this->rendered_fields[0][$options['image']]);
    $image = file_create_url($image_data->uri);
    $output = '';

    // Add legend to top if set to do so.
    if ($legend != NULL && strpos($options['optional_fields']['legend_category_attach'], 'top') !== FALSE) {
      $output .= $legend;
    }
    $output .= '<div class="floorplan-canvas" style="position: relative; 
    display:inline-block; width:' . $options['optional_fields']['width'] . ';" />
    <img src="' . $image . '" style="width:' . $options['optional_fields']['width'] .
    ';height:' . $options['optional_fields']['height'] . ';">
    ';

    // Create the floorplan points.
    foreach ($result as $i => $field) {
      // Create the floorplan point data.
      $x = $field[$options['x_axis']];
      $y = $field[$options['y_axis']];

      // Generate dropdown label.
      $drop_label = (isset($field[$options['optional_fields']['dropdown_label']]) &&
      !empty($field[$options['optional_fields']['dropdown_label']]) &&
      $field[$options['optional_fields']['dropdown_label']] !== 'none') ?
      $field[$options['optional_fields']['dropdown_label']] : FALSE;

      // Generate persistent label.
      $persistent_label = (isset($field[$options['optional_fields']['persistent_label']]) &&
      !empty($field[$options['optional_fields']['persistent_label']]) &&
      $field[$options['optional_fields']['persistent_label']] !== 'none') ?
      $field[$options['optional_fields']['persistent_label']] : FALSE;

      // Get the floorplan color.
      $color = (isset($field[$options['optional_fields']['color_field']]) &&
      !empty($field[$options['optional_fields']['color_field']]) &&
      $field[$options['optional_fields']['color_field']] !== 'none') ?
      $field[$options['optional_fields']['color_field']] : $options['default_color'];
      
      strpos($color, '#') === FALSE ? $color = '#' . $color : NULL;

      // Create the floorplan point.
      $output .= '<div id="floorplan-' . $i . '" 
      class="floorplan-point" style="background-color:' . $color . '; 
      bottom:' . $y . '%; left:' . $x . '%;"></div>';

      if ($persistent_label) {
        $output .= '<div class="floorplan-persistent-label" data-target="floorplan-' . $i . '" 
        style="bottom:' . $y . '%; left:' . $x . '%;">' . $persistent_label . '</div>';
      }

      if ($drop_label) {
        $output .= '<div class="floorplan-dropdown-label" data-parent="floorplan-' . $i . '" 
        style="top:' . (100 - $y) . '%; left:' . $x . '%;"><div class="floorplan-dropdown-contents">' . $drop_label . '<div class="floorplan-close-dropdown" data-target="floorplan-' . $i . '"></div></div></div>';
      }
    }

    // Close the div.
    $output .= '</div>';

    // Attach legend to bottom if set to do so.
    if ($legend != NULL && strpos($options['optional_fields']['legend_category_attach'], 'bottom') !== FALSE) {
      $output .= $legend;
    }
    return $output;
  }

}
