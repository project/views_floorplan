Views Floorplan

Table of Contents

* Introduction
* Module Setup
* Tips when creating views
* Example: Office Seating Chart w/ empty seats
* Contributors

------------------------------------------------------------------------

Introduction

Views Floorplan allows users to create interactive floorplan maps that 
contain the map image, and floorplan points that react to a user 
clicking/touching them. Developers may use conditional logic in Views, 
along with custom CSS/JS to come up with a variety of use cases around 
creting an interactive map or image.

- General logic behind the module

Views Floorplan takes in a set of fields defining the color and location
of "floorplan points," and returns a floorplan image with points 
rendered in place. The image comes from the first row's image field. 
The floorplan points consist of two div elements, one for the point, 
and one for an optional label field.

Additional CSS and JS defining the floorplan points default to resources
provided by the module, but these may be changed by going to 
/admin/config/user-interface/views_floorplan and setting the file paths
of your custom CSS/JS files. The default files add behaviors for points
to "glow" when a mouse hovers over them, and for the label to roll down 
when clicked/touched.

------------------------------------------------------------------------

Module Setup

- Module settings page

The module settings page is located at 
/admin/config/user-interface/views_floorplan upon module install. The 
module currently permits a single set of fields to be used in 
constructing Floorplan style views. Since the module is looking for 
fields, instead of content types, it is possible to have many content 
types/taxonomy vocabularies that use the same fields, if a diverse set 
of floorplan maps is needed.

The module expects the following to be set up first:

1) A "Floorplan" content type, which must a minimum contain an image 
field.
2) A "Floorplan Point" content type, which must contain the following:
-> An Entity Reference field that points to a Floorplan node.
-> A Decimal field for the x-coordinate
-> a Decimal field for the y-coordinate
3) For the View itself, fields for providing labels and colors are 
necessary. The means depends on the use case.

- Views Floorplan Example module

The "Views Floorplan Example" module will provide a default view and 
content types to show the module in action. It will also set automatic 
defaults for the Views Floorplan module. To use it, you will need to 
add the following:

1) Add one "Floorplan" node, and add a floorplan image.
2) Add one or more "Floorplan point" nodes, and add labels and colors.
3) Go to example-floorplan/[nid-of-floorplan-node] and see your 
floorplan!

------------------------------------------------------------------------

Tips when creating Views

- Since we are normally looking for a specific floorplan, we will need 
a contextual filter that provides the NID of the floorplan node. 
Further, we will need a reference of type "Entity Reference: 
Referencing Entity" that will retrieve all floorplan points which 
reference the floorplan.

- Usually it is best to display all tiems with this type of view.

- Start with a table view, check that all fields are providing the 
expected information for the floorplan view before building the 
floorplan view itself. This is especially important for views that use 
conditional logic.

- The Floorplan view style currently will only displays fields that are 
selected through the view settings window. Therefore, "Exclude from 
display" will not behave as expected. Labels still come through, so for 
coordinate fields you will want to avoid labelling them.

- To make rich custom labels, use a Global: Custom Text field for your 
label field. This allows you to put in any number of fields in a 
specified format. For example, an office seating chart may have staff 
profiles.
- The color field is set in the View, so you can use references + 
taxonomy terms to have taxonomy-based colors, a color field within your 
floorplan point node, or conditional logic that provides colors.

------------------------------------------------------------------------

Example: Office Seating Chart w/ empty seats

- Content Setup

Floorplan content type (CT): Office Floorplan
Fields: 
-> Image

Staff: User or Staff CT
Fields:
-> Name
-> Whatever else you'd like for a profile

Floorplan Point CT: Seat
Fields: 
-> Entity Reference to Floorplan CT
-> Entity reference to Staff
-> Decimal for X coordinate
-> Decimal for Y coordinate

Once the above are set up, create a Floorplan Point (Seat) for every 
location in the florplan. You can use Feeds and a little math if you 
want to get fancy. Add people to the seats as applicable.

- Module setup

Add in the fields you created on the Views Floorplan settings page.

- View Setup

1) Begin by creating a Page View with a Floorplan style.

2) Add a contextual filter that references the floorplan nid, and a 
reference of entities referencing the floorplan node through the 
applicable entity reference field (point reference).

3) Add another reference, which connects the floorplan point to the 
Staff entity (staff reference).

4) Add these fields:

-> Image field: Set "rewrite the output of this field" to the image fid.
-> [point reference] Staff Entity Reference: Set "rewrite the output of this 
field" to "00FF00" (green) and "No Results Behavior" to "FF0000" (red).
-> [point reference] X coordinate
-> [point reference] Y coordinate
-> [staff reference] Name
-> [staff reference] Additional Fiedls for profile
-> [point reference] Staff Entity Reference (again): For rewriting, 
create a profile for the staff using the above fields. For No Results, 
enter "empty"

5) Set up the Floorplan format settings for the above fields. For the 
default color, use "FF0000" or the same as your "empty" color. For the 
color field, use the Staff reference with rewrites.

6) The result should be a view with red and green dots for your Office 
Floorplan, with green dots displaying profiles and red dots returning 
"empty."

------------------------------------------------------------------------

Contributors

- Maintainers

laboratorymike: developed version 7.x-1.0 of this module. Current maintainer.

https://www.drupal.org/u/laboratory.mike


- Module reviewers

A special thanks to all module reviewers! Thank you for the excellent feedback!

awasson - https://www.drupal.org/u/awasson
- Thanks for the README feedback and feature requests

Pravin Ajaaz - https://www.drupal.org/u/pravin-ajaaz

vineethaw - https://www.drupal.org/u/vineethaw

davidgrayston - https://www.drupal.org/u/davidgrayston
