<?php

/**
 * @file
 * Views plugin settings for the Floorplan style.
 */

/**
 * Implements hook_views_plugins().
 */
function views_floorplan_views_plugins() {
  $plugins = array();
  $plugins['style']['floorplan'] = array(
    'title' => t('Floorplan'),
    'help' => t('Displays rows as floorplan points'),
    'handler' => 'ViewsFloorplanPluginStyleFloorplan',
    'uses row plugin' => TRUE,
    'uses row class' => TRUE,
    'uses options' => TRUE,
    'type' => 'normal',
    'help topic' => 'style-list',
  );
  return $plugins;
}
