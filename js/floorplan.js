/**
 * @file
 * Set of functions that control behavior of floorplan views.
 */

Drupal.behaviors.views_floorplan = {
  attach: function (context, settings) {
    'use strict';
    // On document ready, shift all points to center them over their coordinate points.
    // This sometimes breaks with Quicktabs or other embed instances; Enable by uncommenting.
    /*
    jQuery('.floorplan-point').each(function () {
      var left = Math.round(parseInt(jQuery(this).css('left'), 10));
      var bottom = Math.round(parseInt(jQuery(this).css('bottom'), 10));
      var width = jQuery(this).outerWidth();
      var height = jQuery(this).outerHeight();
      jQuery(this).css('left', left - width / 2);
      jQuery(this).css('bottom', bottom - height / 2);
    });*/

    // Enable user interactions.
    jQuery('.floorplan-dropdown-label').hide();
    jQuery('.floorplan-point').mouseenter(function () {
      jQuery(this).addClass('floorplan-glow');
    });
    jQuery('.floorplan-point').mouseleave(function () {
      jQuery(this).removeClass('floorplan-glow');
    });
    jQuery('.floorplan-point').click(function () {
      jQuery('[data-parent="' + this.id + '"]').slideToggle();
    });
    jQuery('.floorplan-close-dropdown, .floorplan-persistent-label').click(function () {
      jQuery('[data-parent="' + jQuery(this).attr('data-target') + '"]').slideToggle();
    });
    jQuery('.floorplan-legend-collapse-button').click(function () {
      jQuery('.floorplan-legend').fadeToggle();
    });
  }
};
