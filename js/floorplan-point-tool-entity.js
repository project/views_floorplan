/**
 * @file
 * Set of functions for the floorplan point editor in node/edit forms.
 */

var xField = '';
var yField = '';

var drawFloorPlanPoint = function (xval, yval, h, w) {
  'use strict';
  var xPosn = Math.round(w / 100 * xval);
  var yPosn = Math.round(h / 100 * yval);
  var pointH = jQuery('#point-location').outerHeight();
  var pointW = jQuery('#point-location').outerWidth();

  // Uncomment for clicks to move the bottom-left corner of the floorplan point to the click point.
  jQuery('#point-location').css('left', xPosn);
  jQuery('#point-location').css('bottom', yPosn);

  // Uncomment for clicks to "center" at the point of the mouse click
  /*
  jQuery('#point-location').css('left', xPosn - pointW / 2);
  jQuery('#point-location').css('bottom', yPosn - pointH / 2);
  */

  // Write the output in the x/y box.
  jQuery('input[name*="' + xField + '"]').val(xval);
  jQuery('input[name*="' + yField + '"]').val(yval);
};

Drupal.behaviors.views_floorplan = {
  attach: function (context, settings) {
    'use strict';
    // You can get field names by using 'x_field' in place of 'x_start'
    xField = Drupal.settings.views_floorplan.x_field;
    yField = Drupal.settings.views_floorplan.y_field;
    var xStart = Drupal.settings.views_floorplan.x_start;
    var yStart = Drupal.settings.views_floorplan.y_start;
    var h = jQuery('#floorplan-image').height();
    var w = jQuery('#floorplan-image').width();

    // Draw the point with the given info.
    drawFloorPlanPoint(xStart, yStart, h, w);
  }
};

var floorplanXY = function (e) {
  'use strict';
  // Get the coordinates.
  var offset = jQuery('#floorplan-image').offset();
  var h = jQuery('#floorplan-image').height();
  var w = jQuery('#floorplan-image').width();
  var xPosn = Math.round(e.pageX - offset.left);
  var yPosn = Math.round(-1 * (e.pageY - offset.top - h));
  var xval = (xPosn * 100 / w).toFixed(1);
  var yval = (yPosn * 100 / h).toFixed(1);
  // Draw the point with the given info.
  drawFloorPlanPoint(xval, yval, h, w);
};
